package org.academiadecodigo.shhoncos.emufarm;

public class Bird {

    private int eggsLaid;
    private String name;

    public Bird(String name){
        this.name = name;
        eggsLaid = 0;
    }

    public void layEgg(){
        eggsLaid++;
    }

    public void chirp(){
        System.out.println("Piu piu's e cenas");
    }

    public int getEggsLaid(){
        return eggsLaid;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
