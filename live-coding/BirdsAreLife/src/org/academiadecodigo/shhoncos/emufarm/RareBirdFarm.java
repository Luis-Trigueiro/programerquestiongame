package org.academiadecodigo.shhoncos.emufarm;

public class RareBirdFarm {

    public static void main(String[] args) {
        // Code starts executing here
        Emu mySweetLittleEmu = new Emu();
        mySweetLittleEmu.setName("Worraz");

        Crow snow = new Crow("Snow");
        snow.kill();


        Parrot jack = new Parrot("Jack");
        jack.layEgg();


        jack.chirp();
        mySweetLittleEmu.layEgg();
        mySweetLittleEmu.chirp();

        Bird bird = new Bird("Caçador");
        bird.layEgg();
        System.out.println(mySweetLittleEmu.getName() + " has laid " + mySweetLittleEmu.getEggsLaid() + " eggs." );

    }
}
