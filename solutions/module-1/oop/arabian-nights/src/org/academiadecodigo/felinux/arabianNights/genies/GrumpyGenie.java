package org.academiadecodigo.felinux.arabianNights.genies;

public class GrumpyGenie extends Genie {

    public GrumpyGenie(int maxWishes) {
        super(maxWishes);
    }

    @Override
    public boolean hasWishesToGrant() {
        return (super.getNumberOfWishesGranted() == 0);

    }
}
