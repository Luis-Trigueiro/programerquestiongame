package org.academiadecodigo.felinux.polymorphism;

import org.academiadecodigo.felinux.polymorphism.birds.Bird;
import org.academiadecodigo.felinux.polymorphism.birds.Crow;
import org.academiadecodigo.felinux.polymorphism.birds.Emu;
import org.academiadecodigo.felinux.polymorphism.birds.Parrot;

public class RareBirdFarm {

    public static void main(String[] args) {

        // upcast (always safe)
        Bird anotherBird = new Crow("Crowford");

        // type-safe downcast
        if (anotherBird instanceof Crow) {
            Crow myBeautifulCrow = (Crow) anotherBird;
            myBeautifulCrow.kill();
        }

        // type-unsafe downcast
        // Crow myCrow = (Crow) someBird;
        // myCrow.kill();

        int farmCapacity = 5;
        Bird[] farm = new Bird[farmCapacity];

        for (int index = 0; index < farm.length; index++) {
            farm[index] = createBird();
        }

        System.out.println("\n### MAKING THEM SPEAK ###\n");


        for (Bird bird : farm) {

            if (bird instanceof Crow) {
                ((Crow) bird).kill();
                continue;
            }

            System.out.print(bird.getName() + ": ");
            bird.chirp();
        }
    }

    private static Bird createBird() {

        int random = (int) (Math.random() * 3); // magic number. avoid doing this! what could i use here instead?

        switch (random) {
            case 0:
                return new Crow("Snow");
            case 1:
                return new Emu("Worraz");
            default:
                return new Parrot("Jack");
        }
    }
}
