package org.academiadecodigo.shhoncos.emufarm;

public class Crow extends Bird{

    private int murderVictims;

    public Crow(String name){
       super(name);
    }

    public void kill(){
        murderVictims++;
    }

    @Override
    public void chirp(){
        System.out.println("CAW CAW CAW");
    }
}
