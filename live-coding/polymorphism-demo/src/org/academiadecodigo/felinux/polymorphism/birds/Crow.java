package org.academiadecodigo.felinux.polymorphism.birds;

public class Crow extends Bird {

    private int murderVictims;

    public Crow(String name) {
        super(name);
    }

    public void kill() {
        murderVictims++;
        System.out.println(this.getClass().getSimpleName() + ": another day, another kill");
    }

    @Override
    public void chirp() {
        System.out.println("*CAW CAW CAW*\n  looking for my next victim... ");

    }
}