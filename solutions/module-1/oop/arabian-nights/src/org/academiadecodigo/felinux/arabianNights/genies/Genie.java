package org.academiadecodigo.felinux.arabianNights.genies;

public class Genie {

    private int maxWishes;
    private int wishesGranted;

    public Genie(int maxWishes) {
        this.wishesGranted = 0;
        this.maxWishes = maxWishes;
    }

    public int getNumberOfWishesGranted() {
        return wishesGranted;
    }

    public void grantWish() {

        if (!hasWishesToGrant()) {
            System.out.println("Nope. All wishes are already granted.");
            return;
        }

        System.out.println("Wish granted. Enjoy the infinite supply of beer, felinux!");
        wishesGranted++;

    }

    public boolean hasWishesToGrant() {
        return wishesGranted < maxWishes;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
