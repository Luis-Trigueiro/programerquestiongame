package org.academiadecodigo.felinux.polymorphism.birds;

public class Bird {

    private String name;
    private int eggsLaid;

    public Bird(String name){
        this.name = name;
        eggsLaid = 0;
    }

    public void layEgg(){
        eggsLaid++;
        System.out.println(this.getClass().getSimpleName() + ": *lays a single egg*");
    }

    public void chirp(){
        System.out.println("*piu piu piu piu*");
    }

    public String getName(){
        return name;
    }

    public int getEggsLaid(){
        return eggsLaid;
    }

    public void setName(String name){
        this.name = name;
    }

}
