package org.academiadecodigo.bootcamp49.methodsdemo;

public class Emu {

    private int eggsLaid;
    private String color;
    private String name;

    public Emu(String name, String color, int eggsLaid) {
        this.eggsLaid = eggsLaid;
        this.color = color;
        this.name = name;
    }

    public void run(int time) {
        // int time = 10; -> useless now
        System.out.println("*runs graciously for " + time + " minutes*");
    }

    public int add(int number1, int number2) {

        return number1 + number2;
    }

    public double add(double number1, double number2) {

        return number1 + number2;
    }

    public int add(int number1, int number2, int number3) {

        return number1 + number2 + number3;
    }

    public String getName() {
        return this.name;
    }

    public String getColor() { // GETTERS
        return this.color;
    }

    public int getEggsLaid() {
        return this.eggsLaid;
    }

    public void setColor(String color) { // SETTERS
       this.color = color;
    }

}
