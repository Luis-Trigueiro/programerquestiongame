package org.academiadecodigo.felinux.polymorphism.birds;

public class Emu extends Bird {

    public Emu(String name) {
        super(name);
    }

    @Override
    public void chirp() {
        System.out.println("*making emu sounds, whatever that means*");
    }
}
