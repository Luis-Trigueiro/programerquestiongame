package org.academiadecodigo.bootcamp49.methodsdemo;

public class BirdFarm {

    public static void main(String[] args) {

        Emu superEmu = new Emu("Worraz", "beige", 5);
        Emu megaEmu = new Emu("Patrício", "pink", 420);

        System.out.println(superEmu.getName() + " is " + superEmu.getColor() + " and was born with already " + superEmu.getEggsLaid() + " eggs laid.");

        superEmu.setColor("turquoise");

        System.out.println(superEmu.getName() + " is " + superEmu.getColor() + " and was born with already " + superEmu.getEggsLaid() + " eggs laid.");

        int result = superEmu.add(7, 111);
        superEmu.run(result);
    }
}

