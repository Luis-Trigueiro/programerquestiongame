package org.academiadecodigo.felinux.polymorphism.birds;

public class Parrot extends Bird {

    public Parrot(String name){
        super(name);
    }

    @Override
    public void layEgg(){
        super.layEgg();
        System.out.println("\t OUCH!!!!");
    }
}
