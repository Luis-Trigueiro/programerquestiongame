package org.academiadecodigo.felinux.arabianNights;

import org.academiadecodigo.felinux.arabianNights.genies.Genie;

public class ArabianNights {

    public static void main(String[] args) {

        Lamp aladdin = new Lamp(3);

        Genie[] genies = new Genie[5];

        // after this we populate our array by rubbing the lamp
        System.out.println("\n----- Created a lamp, now rubbing it -----"
                + "\n====================================================");

        for (int i = 0; i < genies.length; i++) {
            genies[i] = aladdin.rub();
        }

        System.out.println("\n----- Recharging a lamp with every genie created  -----"
                + "\n====================================================");

        for (Genie genie : genies) {
            aladdin.recharge(genie);
        }

        System.out.println("\n----- Asking every genie for 1 wish -----"
                + "\n====================================================");

        for (Genie genie : genies) {
            genie.grantWish();
        }

        System.out.println("\n----- Creating a new lamp -----"
                + "\n====================================================");

        // instantiate another lamp
        Lamp willSmith = new Lamp(1);

        System.out.println("\n----- Comparing both lamps -----"
                + "\n====================================================");

        // compare the lamps
        System.out.println("The lamps are equal: " + willSmith.equals(aladdin) + "!");

    }

}
